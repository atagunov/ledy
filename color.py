import enum

def sq_diff(a, b):
    d = a - b
    return d * d


class Color:
    def __init__(self, rgb=None, hsv=None):
        assert rgb or hsv
        assert bool(rgb) != bool(hsv)
        assert isinstance(rgb, (_ColorRGB, type(None))), "Invalid type %s" % type(rgb)
        assert isinstance(hsv, (_ColorHSV, type(None))), "Invalid type %s" % type(hsv)
        self._rgb = rgb
        self._hsv = hsv

    def __str__(self):
        return str(self._rgb) if self._rgb else str(self._hsv)

    __repr__ = __str__

    def __eq__(self, other):
        if isinstance(other, Color):
            if self._rgb is not None and other._rgb is not None:
                return self._rgb == other._rgb
            if self._hsv is not None and other._hsv is not None:
                return self._hsv == other._hsv
            return self.rgb == other.rgb
        if isinstance(other, _ColorRGB):
            return self.rgb == other

        if isinstance(other, _ColorHSV):
            return self.hsv == other
        return False

    @classmethod
    def make_rgb(cls, red=0, green=0, blue=0, hex_rgb=None):
        if hex_rgb:
            return _ColorRGB.from_str(hex_rgb)
        return cls(_ColorRGB(red, green, blue))

    @classmethod
    def make_hsv(cls, hue, saturation, value):
        return cls(hsv=_ColorHSV(hue, saturation, value))

    @property
    def rgb(self):
        if self._rgb:
            return self._rgb
        self._rgb = self._hsv.to_rgb()
        return self._rgb

    @property
    def hsv(self):
        if self._hsv:
            return self._hsv
        self._hsv = _ColorHSV.from_rgb(self._rgb)
        return self._hsv

    def get_color_code(self):
        return self.rgb.get_color_code()

    def copy(self):
        if self._rgb:
            c = Color(rgb=self._rgb)
            if self._hsv:
                c._hsv = self._hsv
        else:
            c = Color(hsv=self._hsv)
        return c

    def distance2(self, other, euclidean=False):
        # return square of distance of two colors
        rgb = self.rgb
        other_rgb = other.rgb
        k = (1, 1, 1) if euclidean else (2, 4, 3)
        return \
            k[0] * sq_diff(rgb.red, other_rgb.red) + \
            k[1] * sq_diff(rgb.green, other_rgb.green) + \
            k[2] * sq_diff(rgb.blue, other_rgb.blue)


class _ColorScheme(object):
    def to_rgb(self):
        pass

    @classmethod
    def from_rgb(cls, rgb):
        pass

    @classmethod
    def from_str(cls, s):
        pass

    def get_color_code(self):
        pass

    def _assert_range(self, value, r1, r2):
        assert isinstance(value, int)
        assert r1 <= value <= r2, "value %s should be in range %s - %s " % (value, r1, r2)


class _ColorRGB(_ColorScheme):
    def __init__(self, red=0, green=0, blue=0, white=0):
        self.red = red  # [0, 255]
        self.green = green  # [0, 255]
        self.blue = blue  # [0, 255]
        self.white = white
        self._assert_range(self.red, 0, 255)
        self._assert_range(self.green, 0, 255)
        self._assert_range(self.blue, 0, 255)

    def __str__(self):
        return "#%02x%02x%02x" % (self.red, self.green, self.blue)

    __repr__ = __str__

    def copy(self):
        return _ColorRGB(self.red, self.green, self.blue, self.white)

    @staticmethod
    def _norm(value):
        return max(min(value, 255), 0)

    def get_color_code(self):
        return (_ColorRGB._norm(self.white) << 24) | \
               (_ColorRGB._norm(self.red) << 16) | \
               (_ColorRGB._norm(self.green) << 8) | \
               _ColorRGB._norm(self.blue)

    def to_rgb(self):
        return self

    @classmethod
    def from_rgb(cls, rgb):
        return rgb

    @classmethod
    def from_str(cls, s):
        if s[0] == "#":
            s = s[1:]
        rgb = int(s, 16)
        rg = rgb // 256
        return cls(rg // 256, rg % 256, rgb % 256)

    def __eq__(self, other):
        if isinstance(other, _ColorRGB):
            return self.red == other.red and self.green == other.green and \
                   self.blue == other.blue
        return False

    def __ne__(self, other):
        return not self.__eq__(other)


class _ColorHSV(_ColorScheme):
    def __init__(self, hue, saturation, value):
        self._assert_range(hue, 0, 359)
        self._assert_range(saturation, 0, 255)
        self._assert_range(value, 0, 255)

        self.hue = hue  # [0, 359]
        self.saturation = saturation  # [0, 255]
        self.value = value  # [0, 255]

    def __str__(self):
        return "<HSV %s %s %s>" % (self.hue, self.saturation, self.value)

    __repr__ = __str__

    def __eq__(self, other):
        if isinstance(other, _ColorHSV):
            return self.hue == other.hue and self.saturation == other.saturation and self.value == other.value
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def copy(self):
        return _ColorHSV(self.hue, self.saturation, self.value)

    def to_rgb(self):
        if self.saturation == 0:
            return _ColorRGB(self.value, self.value, self.value)
        i = self.hue // 60
        base = (255 - self.saturation) * self.value // 256
        if i == 0:
            return _ColorRGB(self.value,
                             (((self.value - base) * self.hue) // 60) + base,
                             base)
        if i == 1:
            return _ColorRGB(
                (((self.value - base) * (60 - (self.hue % 60))) // 60) + base,
                self.value,
                base)

        if i == 2:
            return _ColorRGB(
                base,
                self.value,
                (((self.value - base) * (self.hue % 60)) // 60) + base)

        if i == 3:
            # colors[1] = (((val - base) * (60 - (hue % 60))) / 60) + base;
            return _ColorRGB(
                base,
                (((self.value - base) * (60 - (self.hue % 60))) // 60) + base,
                self.value)

        if i == 4:
            return _ColorRGB(
                (((self.value - base) * (self.hue % 60)) // 60) + base,
                base,
                self.value)

        if i == 5:
            return _ColorRGB(
                self.value,
                base,
                (((self.value - base) * (60 - (self.hue % 60))) // 60) + base)
        assert "Wrong hue %s" % self.hue

    @classmethod
    def from_rgb(cls, rgb):
        """
    maxc = max(r, g, b)
    minc = min(r, g, b)
    v = maxc
    if minc == maxc:
        return 0.0, 0.0, v
    s = (maxc-minc) / maxc
    rc = (maxc-r) / (maxc-minc)
    gc = (maxc-g) / (maxc-minc)
    bc = (maxc-b) / (maxc-minc)
    if r == maxc:
        h = bc-gc
    elif g == maxc:
        h = 2.0+rc-bc
    else:
        h = 4.0+gc-rc
    h = (h/6.0) % 1.0
    return h, s, v

        """
        maxc = max(rgb.red, rgb.blue, rgb.green)
        minc = min(rgb.red, rgb.blue, rgb.green)
        d = maxc - minc
        value = maxc
        if d == 0:
            return cls(0, 0, value)
        s = d * 255 // maxc
        # rc = (maxc - rgb.red) * 360 / d
        # gc = (maxc - rgb.green) * 360 / d
        # bc = (maxc - rgb.blue) * 360 / d
        if rgb.red == maxc:
            hue = 60 * (rgb.green - rgb.blue) // d
        elif rgb.green == maxc:
            hue = 120 + 60 * (rgb.blue - rgb.red) // d
        else:
            hue = 240 + 60 * (rgb.red - rgb.green) // d

        if hue < 0:
            hue += 360
        elif hue >= 360:
            hue -= 360

        return cls(hue, s, value)

    def get_color_code(self):
        return self.to_rgb().get_color_code()


class StandardColors(enum.Enum):
    RED = Color.make_rgb(255, 0, 0)
    GREEN = Color.make_rgb(0, 0x80, 0)
    BLUE = Color.make_rgb(0, 0, 255)
    WHITE = Color.make_rgb(255, 255, 255)
    BLACK = Color.make_rgb(0, 0, 0)

    AQUA = Color.make_rgb(0, 255, 255)
    MAGENTA = Color.make_rgb(255, 0, 255)
    GRAY = Color.make_rgb(0x80, 0x80, 0)
    LIME = Color.make_rgb(0, 255, 0)
    MAROON = Color.make_rgb(0x80, 0, 0)
    NAVY = Color.make_rgb(0, 0, 0x80)
    OLIVE = Color.make_rgb(0x80, 0x80, 0)
    PURPULE = Color.make_rgb(0x80, 0, 0x80)
    SILVER = Color.make_rgb(0xC0, 0xC0, 0xC0)
    TEAL = Color.make_rgb(0, 0x80, 0x80)
    YELLOW = Color.make_rgb(255, 255, 0)
