from __future__ import print_function
import os
import time
from transformer import Cleaner,  SolidLight, SmoothLight, GradientLight
from color import StandardColors, Color


SPEED = 10


def make_strip():
    import neopixel
    LED_COUNT = 50  # Number of LED pixels.
    LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
    # LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
    LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA = 10  # DMA channel to use for generating signal (try 10)
    LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
    LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53

    strip = neopixel.Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    strip.begin()
    return strip


class WinStripMock:
    LAMP_SIZE = 20

    def __init__(self, count, lamp_size=LAMP_SIZE):
        from graphics import GraphWin

        self.lamp_size = lamp_size
        self.lamp_radius = lamp_size // 2
        self.count = count
        size = (lamp_size + lamp_size // 2) * count
        self.win = GraphWin("LED Strip", size + 2 * lamp_size, lamp_size * 2, autoflush=False)
        self.empty_draw()

    def empty_draw(self):
        from graphics import GraphWin, Circle, Point, color_rgb

        center_x = self.lamp_size
        center_y = self.lamp_size

        for i in range(self.count):
            c = Circle(Point(center_x, center_y), self.lamp_radius)
            c.draw(self.win)
            center_x += self.lamp_size + self.lamp_radius
        self.win.update()

    def __len__(self):
        return self.count

    def setPixelColor(self, led, color):
        from graphics import color_rgb, Circle, Point
        color = color_rgb(*self.unpack_color(color))
        c = Circle(Point(self.lamp_size + led * (self.lamp_size + self.lamp_radius), self.lamp_size),
                   self.lamp_radius)
        c.setFill(color)
        c.draw(self.win)

    @staticmethod
    def _get_bits(value, start, end):
        value = value >> start
        mask = (1 << (end - start)) - 1
        return value & mask

    @staticmethod
    def unpack_color(value):
        red = WinStripMock._get_bits(value, 16, 24)
        green = WinStripMock._get_bits(value, 8, 16)
        blue = WinStripMock._get_bits(value, 0, 8)
        if value > 0:
            WinStripMock._get_bits(value, 16, 24)
        return red, green, blue

    def show(self):
        self.win.update()


def make_strip_mock(count=50):
    strip = WinStripMock(count=count)

    return strip


def test_solid(strip):
    print("red")
    cleaner = SolidLight(strip, color=StandardColors.RED.value)
    cleaner.tick(True)
    strip.show()
    time.sleep(1. / SPEED)


def clear(strip):
    cleaner = Cleaner(strip)
    cleaner.tick(True)
    strip.show()
    time.sleep(0.3)


def test_smooth(strip):
    for c in [StandardColors.WHITE, StandardColors.BLUE, StandardColors.RED, StandardColors.GREEN,
              StandardColors.AQUA, StandardColors.MAGENTA, StandardColors.GRAY]:
        c = c.value
        print("Smooth light", c)

        clear(strip)

        start_end = {}
        if os.name == "nt":
            start_end["start"] = 0
            start_end["end"] = len(strip) // SPEED
        sm = SmoothLight(strip, c, 100 // SPEED, **start_end)
        n = 0
        while not sm.tick():
            strip.show()
            time.sleep(0.3 / SPEED)
            n += 1
        strip.show()


def gradient(strip, left, right):
    print("Gradient", left, right)
    sm = GradientLight(strip, left, right)
    while not sm.tick():
        strip.show()
    time.sleep(10 // SPEED)


def main():
    if os.name == 'nt':
        strip = make_strip_mock()
    else:
        strip = make_strip()

    clear(strip)
    test_solid(strip)
    # test_smooth(strip)

    clear(strip)

    for left, right in [(StandardColors.RED, StandardColors.BLUE),
                        (StandardColors.BLUE, StandardColors.RED),
                        (StandardColors.GREEN, StandardColors.RED),
                        ]:
        gradient(strip, left.value, right.value)



if __name__ == '__main__':
    main()
