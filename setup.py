from setuptools import setup

setup(
    name="ledy",
    version="0.1",
    author="Anton Tagunov",
    url="https://bitbucket.org/atagunov/ledy/",
    description="Ledy",
    install_requires=['PyYAML', 'six'],
    tests_require=["pytest"],
    classifiers=[
        "Programming Language :: Python :: 3",
    ]
)
