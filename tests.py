import pytest
from color import Color, StandardColors


def test_color_converting():
    step = 4
    for red in range(0, 256, step):
        for green in range(0, 256, step):
            for blue in range(0, 256, step):
                c = Color.make_rgb(red, green, blue)
                hsv_color = Color(hsv=c.hsv)
                converted_rgb = Color(rgb=hsv_color.rgb)
                diff = c.distance2(converted_rgb, euclidean=True)
                assert diff <= 26, "distance is too large between double converted color %s-%s (hsv %s): %s" % \
                                   (c, converted_rgb, c.hsv, diff)


def test_gradient_standart():
    for color1 in StandardColors:
        for color2 in StandardColors:
            if color1 != color2:
                print(color1, color2)
