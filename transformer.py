# -*- coding: utf-8 -*-
from color import StandardColors, Color


class Transformer(object):
    def __init__(self, led_strip, start=None, end=None, **kwargs):
        self.led_strip = led_strip
        self.kwargs = kwargs
        self.step = 0
        self.start = 0 if start is None else start
        self.end = len(led_strip) if end is None else end

    def tick(self, force=False):
        # returns True when transformation was completed
        self.step += 1
        return True

    def get_delay(self):
        return None

    def _str_args(self):
        return ""

    def __str__(self):
        return "<%s %s-%s (%s) %s>" % (self.__class__.__name__,
                                       self.start, self.end,
                                       self.step, self._str_args())


class SolidLight(Transformer):
    def __init__(self, led_strip, color, **kwargs):
        self.color = color
        super(SolidLight, self).__init__(led_strip, **kwargs)

    def _str_args(self):
        return str(self.color)

    def tick(self, force=False):
        if self.step == 0 or force:
            for led in range(self.start, self.end):
                self.led_strip.setPixelColor(led, self.color.get_color_code())
        super(SolidLight, self).tick(force)
        return True


class Cleaner(SolidLight):
    def __init__(self, led_strip, color=StandardColors.BLACK.value, **kwargs):
        super(Cleaner, self).__init__(led_strip, color=color, **kwargs)


class SmoothLight(Transformer):
    def __init__(self, led_strip, color, steps, **kwargs):
        super(SmoothLight, self).__init__(led_strip, **kwargs)
        self.color = color
        self.steps = steps
        self.hsv_color = color.hsv
        self.value_by_step = self.hsv_color.value / self.steps

    def _str_args(self):
        return str(self.color)

    def tick(self, force=False):
        color = self.hsv_color.copy()
        if self.step < self.steps or force:
            color.value = int(self.value_by_step * min(self.step + 1, self.steps))
            for led in range(self.start, self.end):
                self.led_strip.setPixelColor(led, color.get_color_code())

        super(SmoothLight, self).tick(force)
        return self.step > self.steps


class GradientLight(Transformer):
    def __init__(self, led_strip, color_first, color_second, **kwargs):
        super(GradientLight, self).__init__(led_strip, **kwargs)
        print("!!!", color_first, color_second)
        self.c1 = color_first.hsv
        self.c2 = color_second.hsv
        self.steps = self.end - self.start
        self._interval_range = 256
        self._half_interval_range = self._interval_range // 2

    def _str_args(self):
        return "%s-%s" % (self.c1, self.c2)

    @staticmethod
    def _inner_point(left, right, t):
        return left + (t * (right - left) + 128) // 256

    def inner_color(self, step):
        c1 = self.c1
        c2 = self.c2
        c1_hue = self.c1.hue
        c2_hue = self.c2.hue
        hue_diff = c2_hue - c1_hue
        t = (step * 255) // (self.steps-1)
        if hue_diff < 0:
            # swap hue
            hue_diff = -hue_diff
            t = 255 - t
            c1_hue, c2_hue = c2_hue, c1_hue
            c1, c2 = c2, c1

        if hue_diff > self._half_interval_range:  # 180deg
            c1_hue += 360  # 360 deg
            hue = self._inner_point(c1_hue, c2_hue, t) % 360
            # остаток от деления!!!
            # h = ( a.h + t * (b.h - a.h) ) % 1; // 360deg
        else:
        # if hue_diff <= self._half_interval_range:
            hue = self._inner_point(c1_hue, c2_hue, t)

        return Color.make_hsv(
            hue,
            self._inner_point(c1.saturation, c2.saturation, t),
            self._inner_point(c1.value, c2.value, t),
        )

    def tick(self, force=False):
        super(GradientLight, self).tick(force)

        if self.step == 1 or force:
            for led in range(self.start, self.end):
                c = self.inner_color(led - self.start)
                self.led_strip.setPixelColor(led, c.get_color_code())
            return False
        return True
